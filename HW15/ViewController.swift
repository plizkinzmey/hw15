//
//  ViewController.swift
//  HW15
//
//  Created by Александр Бабкин on 04.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let arrayOriginInt = [1,7,12,56,3,8,16,0,4,17]
        let arayOriginName = ["Alex", "Sergey", "Olia", "Maksim"]
        
        
        // Отсортируйте массив чисел по возрастанию, используя функцию Sorted.
        let arraySorted = arrayOriginInt.sorted()
        
        // Переведите массив чисел в массив строк с помощью функции Map.
        let arrayMapIntToString = arrayOriginInt.map { "\($0)"}
        
        // Переведите массив строк с именами людей в одну строку, содержащую все эти имена, с помощью функции Reduce.
        let arraReduceName = arayOriginName.reduce ("") { names, name in name + " " + names}
        
        // Напишите функцию, которая принимает в себя функцию c типом (Void) -&gt; Void, которая будет вызвана с задержкой в две секунды.
        
        func sleepTwoSeconds() {
            let seconds = 2.0
            func printText() {
                print("Two seconds!")
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                printText()
            }
            
            
        }
        
        
        // Напишите функцию, которая принимает в себя две функции и возвращает функцию, которая при вызове выполнит первые две функции.
        
        func leftText () -> String {
            return "Hello"
        }
        
        func rightText() -> String {
            return "World!"
        }
        
        func getResult() -> (String) {
            
            func sumText() -> String {
                return "\(leftText()) \(rightText())"
            }
            
            return sumText()
        }
        
        
        // Напишите функцию, которая сортирует массив по переданному алгоритму: принимает в себя массив чисел и функцию, которая берёт на вход два числа, возвращает Bool (должно ли первое число идти после второго) и возвращает массив, отсортированный по этому алгоритму.
        
        func sortedArray(array: [Int], alg: String) {
            var sortedArray: [Int]?
            if alg == "<" {
                sortedArray = array.sorted(by: {$0 < $1})
            }
            if alg == ">" {
                sortedArray = array.sorted(by: {$0 > $1})
            }
            print(sortedArray!)
        }
        
        
        
        // Напишите своими словами, что такое infix-, suffix-, prefix-операторы.
        // В Swift мы можем реализовывать свои собственные операторы. Для этого мы используем ключевое слово "operator" и помечаем модификаторами "infix", "suffix", "prefix". Пример: "prefix operator".
        // "prefix" оператор будет использоваться перед своим операндом. Пример: a = !a
        // "postfix" оператор будет использоваться после своего операнда. Пример: a = a!
        // "infix" оператор будет использоваться между своими операндами. Пример: a = a + b. так же infix - оператор может иметь группу приоритета выполнения.
        // про "suffix" ничего не нашел :(
        
        // Напишите своими словами, что такое Pure Function.
        // "чистая функция" - функция, которая при передаче в нее одинакового набора данных, всегда возвращает один и тот же результат. А так же данная функция не вносит изменения в глобальные переменные.
        
        
        
        // вывод в лог
        
        print(arrayOriginInt)
        print(arayOriginName)
        print(arraySorted)
        print(arrayMapIntToString)
        print(arraReduceName)
        sleepTwoSeconds()
        print(getResult())
        sortedArray(array: arrayOriginInt, alg: ">")
        sortedArray(array: arrayOriginInt, alg: "<")
        
    }
}
